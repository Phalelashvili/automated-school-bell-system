import sys, ctypes, json
from tkinter import *
import tkinter.ttk as ttk
from time import sleep
import serial.tools.list_ports
from threading import Thread
from os import system as cmd
from os import _exit as _exit
from time import localtime as time
from requests import get
from win10toast import ToastNotifier
from tkinter import *

setup_finished, arduino_recognized = None, None

def log(textToPrint):
    print(textToPrint)

if len(sys.argv) > 1:
    if sys.argv[1] == 'debug':
        def log(textToPrint):
            with open("debug.log", "a") as f:
                f.write(textToPrint + '\n')
    elif sys.argv[1] == 'update': # force update
        sketchVersion = 0       # if any argument other than update is supplied, program will exit, since time has been set to arduino already, DS1302 isn't accurate enough 
        exeVersion = 0          # i added argument to the shortcut of exe which is in startup folder, so whole program wouldn't open
    else:
        if len(sys.argv) > 2:
            if sys.argv[2] == 'debug':
                def log(textToPrint):
                    with open("debug.log", "a") as f:
                        f.write(textToPrint + '\n')

def messageBox(title, message):
    ctypes.windll.user32.MessageBoxW(0, u"%s"%message, u"%s"%title, 0)

def update(): # checking for updates of exe and sketch
    global ard, setup_finished, arduino_recognized
    try:
        while setup_finished == None: pass
        while arduino_recognized != True:
            if arduino_recognized == False:
                sys.exit(0)
        new = json.loads(get('https://gitlab.com/Phalelashvili/automated-school-bell-system/raw/master/versions.json', timeout=5).content.decode('UTF-8'))
        if sketchVersion != new['sketchVersion']:
            log('Getting updated sketch...')
            new_code = get('https://gitlab.com/Phalelashvili/automated-school-bell-system/raw/master/Windows_Executable/source_code.hex', timeout=5).content.decode('UTF-8')
            with open('source_code.hex', 'w') as f:
                f.write(new_code)
            try: # update is multithreaded, it might execute before arduino is even connected
                ard.close() # will raise not defined error if arduino isn't connected
            except:
                log('Waiting for arduino to connect')
            log('Uploading updated sketch to arduino...')
            cmd('avrdude -v -patmega328p -carduino -P%s -b57600 -D -Uflash:w:source_code.hex 2>> debug.log' % com.upper()) # uploading HEX (already compiled code, not cpp) to arduino via avrdude, has to be in same folder
            log('Sketch updated')
            ard = serial.Serial(com, 9600)
            with open('versions.json', 'w') as versions:
                json.dump({"exeVersion": exeVersion, "sketchVersion": new['sketchVersion']}, versions)
        else:
            log('Sketch up to date')
        if exeVersion != new['exeVersion']:
            log('Updating executable...')
            exe = get('https://gitlab.com/Phalelashvili/automated-school-bell-system/raw/master/Windows_Executable/bell.exe', timeout=5)
            with open('bell_updated.exe', 'wb') as f:
                for chunk in exe.iter_content(chunk_size=1024):
                    if chunk:
                        f.write(chunk)
            with open('update.bat', 'w') as f:
                f.write('''@echo off
if exist bell_updated.exe (
    timeout 2
    del bell.exe
    rename "bell_updated.exe" "bell.exe"
) else (
    del versions.json
)
del "%~f0" & exit 0
)''')
            log('Executable updated')
            with open('versions.json', 'w') as versions:
                json.dump({"exeVersion": new['exeVersion'], "sketchVersion": sketchVersion}, versions)
            cmd('start /MIN update.bat')
            _exit(1)
        else:
            log('EXE up to date')
    except Exception as err:
        log('Update failed' + str(err))

Thread(target=update).start()

def upload(): # update durations of lesson, break and bell
    while not ard.is_open: pass
    easter_egg() # don't mind this
    lesson = int(lesson_length.get())
    if len(str(lesson)) == 1: # same thing i did before, adding 0 if value is only 1 digit
        lesson = '0' + str(lesson)
    elif len(str(lesson)) > 2:
        messageBox("შეცდომა", "99ზე მეტია")
        return
    breakk = int(break_length.get())
    if len(str(breakk)) == 1:
        breakk = '0' + str(breakk)
    elif len(str(breakk)) > 2:
        return
    bell = int(bell_length.get())
    if len(str(bell)) == 1:
        bell = '0' + str(bell)
    elif len(str(bell)) > 2:
        messageBox("შეცდომა", "99ზე მეტია")
        return
    longbell =  int(long_bell_length.get())
    if len(str(longbell)) == 1:
        longbell = '0' + str(longbell)
    elif len(str(longbell)) > 2:
        messageBox("შეცდომა", "99ზე მეტია")
        return
    data_to_send = 'drtn,%s,%s,%s,%s' % (lesson, breakk, bell, longbell)
    ard.write(data_to_send.encode('UTF-8'))
    messageBox("ზარი", "პარამეტრები შეინახა")
    log("Data sent to %s" % com)

def easter_egg():
    try:
        cmd('start gelikos_tomi.png')
    except:
        log('easter egg pic not found')

try:
    with open('versions.json') as versions:
        j = json.load(versions)
    sketchVersion = j['sketchVersion']
    exeVersion = j['exeVersion']
except:
    sketchVersion = 0
    exeVersion = 0

received_data = ''
t = 0

toaster = ToastNotifier()

ports = list(serial.tools.list_ports.comports()) # get list of connected devices
for com,port,details in ports: 
    if 'CH340' in port: # CH340 is chip of chinese arduino clones, you should get real name of chip if you're using something else
        try:
            ard = serial.Serial(com, 9600)
            log(com)
        except Exception as err:
            log(err)
            messageBox("შეცდომა", "Access Denied")
            sys.exit(0)
        log('Arduino found on %s (%s, %s)' % (com, port, details))
        while ard.inWaiting() == 0: # script freezes until bytes arrive
            log('Waiting for bytes')  # arduino sends default values used later
            sleep(1)
            t += 1
            if t > 10:
                arduino_recognized = True
                setup_finished = False
                log('failed receiving bytes')
                sys.exit(0)
        incomingBytes = ard.inWaiting() # we need to know amount of bytes we should read
        log('bytes ' + str(incomingBytes))
        received_data = ard.read(incomingBytes).decode('UTF-8').split(',')
        log(str(received_data))
        year, month, day, hour, minute, second, dow, useless, useless2 = time()
        if len(str(month)) == 1:      # arduino uses .substring function to tell difference between different values
            month = '0' + str(month) # so we need to add 0 if value is only one digit
        if len(str(day)) == 1:
            day = '0' + str(day)
        if len(str(hour)) == 1:
            hour = '0' + str(hour)
        if len(str(minute)) == 1:
            minute = '0' + str(minute)
        if len(str(second)) == 1:
            second = '0' + str(second)
        data_to_send = 'time,%s,%s,%s,%s,%s,%s,%s' % (year, month, day, hour, minute, second, dow)
        ard.write(data_to_send.encode('UTF-8'))
        toaster.show_toast("ზარი", "დრო დაყენდა %s:%s:%s" % (hour, minute, second), icon_path="icon.ico", threaded=True) # throw windows 10 notification
        arduino_recognized = True
        setup_finished = True
        break
    else:
        log('Ignored %s (%s, %s)' % (com, port, details))



if len(sys.argv) > 1:
    if sys.argv[1] == 'setTime':
        sys.exit(0)
if arduino_recognized != True:
    setup_finished = False
    arduino_recognized = False
elif not arduino_recognized:
    messageBox("შეცდომა", "შეერთებული არაა")
    sys.exit(0)


def set_Tk_var():
    global lesson_length
    lesson_length = StringVar()
    lesson_length.set(received_data[0])
    global break_length
    break_length = StringVar()
    break_length.set(received_data[1])
    global bell_length
    bell_length = StringVar()
    bell_length.set(received_data[2])
    global long_bell_length
    long_bell_length = StringVar()
    long_bell_length.set(received_data[3])

def init(top, gui, *args, **kwargs):
    global w, top_level, root
    w = gui
    top_level = top
    root = top

def destroy_window():
    global top_level
    top_level.destroy()
    top_level = None

# if __name__ == '__main__':
#     vp_start_gui()

# no comments here, i used http://page.sourceforge.net/ to create gui, i hate building gui/front-end
def vp_start_gui():
    '''Starting point when module is the main routine.'''
    global val, w, root
    root = Tk()
    set_Tk_var()
    top = ______________ (root)
    init(root, top)
    try:
        root.iconbitmap(r'icon.ico')
    except:
        pass
    root.mainloop()

w = None
def create_______________(root, *args, **kwargs):
    '''Starting point when module is imported by another program.'''
    global w, w_win, rt
    rt = root
    w = Toplevel (root)
    set_Tk_var()
    top = ______________ (w)
    init(w, top, *args, **kwargs)
    return (w, top)

def destroy_______________():
    global w
    w.destroy()
    w = None


class ______________:
    def __init__(self, top=None):
        '''This class configures and populates the toplevel window.
           top is the toplevel containing window.'''
        _bgcolor = '#d9d9d9'  # X11 color: 'gray85'
        _fgcolor = '#000000'  # X11 color: 'black'
        _compcolor = '#ffffff' # X11 color: 'white'
        _ana1color = '#ffffff' # X11 color: 'white' 
        _ana2color = '#ffffff' # X11 color: 'white' 

        top.geometry("303x226+609+197")
        top.title("ზარის მართვა")
        top.configure(background="#fff")
        top.configure(highlightbackground="#d9d9d9")
        top.configure(highlightcolor="black")



        self.gakv = Entry(top)
        self.gakv.place(relx=0.033, rely=0.221,height=20, relwidth=0.442)
        self.gakv.configure(background="white")
        self.gakv.configure(disabledforeground="#a3a3a3")
        self.gakv.configure(font="TkFixedFont")
        self.gakv.configure(foreground="#000000")
        self.gakv.configure(highlightbackground="#d9d9d9")
        self.gakv.configure(highlightcolor="black")
        self.gakv.configure(insertbackground="black")
        self.gakv.configure(selectbackground="#c4c4c4")
        self.gakv.configure(selectforeground="black")
        self.gakv.configure(takefocus="0")
        self.gakv.configure(textvariable=lesson_length)

        self.gakv_label = Label(top)
        self.gakv_label.place(relx=0.033, rely=0.044, height=31, width=134)
        self.gakv_label.configure(activebackground="#f9f9f9")
        self.gakv_label.configure(activeforeground="black")
        self.gakv_label.configure(background="#fff")
        self.gakv_label.configure(disabledforeground="#a3a3a3")
        self.gakv_label.configure(foreground="#000000")
        self.gakv_label.configure(highlightbackground="#d9d9d9")
        self.gakv_label.configure(highlightcolor="black")
        self.gakv_label.configure(text='''გაკვეთილის
ხანგრძლივობა (წთ)''')

        self.Save = Button(top)
        self.Save.place(relx=0.198, rely=0.664, height=64, width=187)
        self.Save.configure(activebackground="#d9d9d9")
        self.Save.configure(activeforeground="#000000")
        self.Save.configure(background="#d9d9d9")
        self.Save.configure(command=upload)
        self.Save.configure(cursor="hand2")
        self.Save.configure(disabledforeground="#a3a3a3")
        self.Save.configure(foreground="#000000")
        self.Save.configure(highlightbackground="#d9d9d9")
        self.Save.configure(highlightcolor="black")
        try:
            self._img1 = PhotoImage(file="rati.png")
            self.Save.configure(image=self._img1)
        except:
            log('pic not found')
        self.Save.configure(pady="0")
        self.Save.configure(takefocus="0")
        self.Save.configure(text='''შენახვა''')

        self.dasv_label = Label(top)
        self.dasv_label.place(relx=0.528, rely=0.044, height=41, width=134)
        self.dasv_label.configure(activebackground="#f9f9f9")
        self.dasv_label.configure(activeforeground="black")
        self.dasv_label.configure(background="#fff")
        self.dasv_label.configure(disabledforeground="#a3a3a3")
        self.dasv_label.configure(foreground="#000000")
        self.dasv_label.configure(highlightbackground="#d9d9d9")
        self.dasv_label.configure(highlightcolor="black")
        self.dasv_label.configure(text='''დიდი დასვენების
ხანგრძლივობა (წთ)''')

        self.dasv = Entry(top)
        self.dasv.place(relx=0.528, rely=0.221,height=20, relwidth=0.442)
        self.dasv.configure(background="white")
        self.dasv.configure(disabledforeground="#a3a3a3")
        self.dasv.configure(font="TkFixedFont")
        self.dasv.configure(foreground="#000000")
        self.dasv.configure(highlightbackground="#d9d9d9")
        self.dasv.configure(highlightcolor="black")
        self.dasv.configure(insertbackground="black")
        self.dasv.configure(selectbackground="#c4c4c4")
        self.dasv.configure(selectforeground="black")
        self.dasv.configure(takefocus="0")
        self.dasv.configure(textvariable=break_length)

        self.zari = Entry(top)
        self.zari.place(relx=0.033, rely=0.487,height=20, relwidth=0.442)
        self.zari.configure(background="white")
        self.zari.configure(disabledforeground="#a3a3a3")
        self.zari.configure(font="TkFixedFont")
        self.zari.configure(foreground="#000000")
        self.zari.configure(highlightbackground="#fff")
        self.zari.configure(highlightcolor="black")
        self.zari.configure(insertbackground="black")
        self.zari.configure(selectbackground="#e6e6e6")
        self.zari.configure(selectforeground="black")
        self.zari.configure(takefocus="0")
        self.zari.configure(textvariable=bell_length)

        self.zari2 = Entry(top)
        self.zari2.place(relx=0.528, rely=0.487,height=20, relwidth=0.442)
        self.zari2.configure(background="white")
        self.zari2.configure(disabledforeground="#a3a3a3")
        self.zari2.configure(font="TkFixedFont")
        self.zari2.configure(foreground="#000000")
        self.zari2.configure(highlightbackground="#fff")
        self.zari2.configure(highlightcolor="black")
        self.zari2.configure(insertbackground="black")
        self.zari2.configure(selectbackground="#e6e6e6")
        self.zari2.configure(selectforeground="black")
        self.zari2.configure(takefocus="0")
        self.zari2.configure(textvariable=long_bell_length)

        self.Label1 = Label(top)
        self.Label1.place(relx=0.033, rely=0.31, height=31, width=129)
        self.Label1.configure(activebackground="#ffffff")
        self.Label1.configure(activeforeground="black")
        self.Label1.configure(background="#fff")
        self.Label1.configure(disabledforeground="#a3a3a3")
        self.Label1.configure(foreground="#000000")
        self.Label1.configure(highlightbackground="#fff")
        self.Label1.configure(highlightcolor="black")
        self.Label1.configure(text='''ზარის
ხანგრძლივობა (წმ)''')

        self.Label2 = Label(top)
        self.Label2.place(relx=0.528, rely=0.31, height=36, width=131)
        self.Label2.configure(activebackground="#ffffff")
        self.Label2.configure(activeforeground="black")
        self.Label2.configure(background="#fff")
        self.Label2.configure(disabledforeground="#a3a3a3")
        self.Label2.configure(foreground="#000000")
        self.Label2.configure(highlightbackground="#fff")
        self.Label2.configure(highlightcolor="black")
        self.Label2.configure(text='''გრძელი ზარის
ხანგრძლივობა (წმ)''')


if __name__ == '__main__':
    vp_start_gui()


