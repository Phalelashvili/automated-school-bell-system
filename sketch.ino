#include <LiquidCrystal.h>
#include <DS1302.h>
#include <EEPROM.h>

String currentState = ""; // just once xD

unsigned long previous = 0;
int current, lesson, lessonrt, breakrt, bellms, bellrt, longbellms, longbellrt, kinda_unix, weli;
long remainingtime, len, target_time, fullLesson, fullBreak, fullLongBreak, minutes;

bool pre, finished, broken_time, backup_method_enabled = false;
bool lesson1_ran, break1_ran, lesson2_ran, break2_ran, lesson3_ran, break3_ran, lesson4_ran, break4_ran, lesson5_ran, break5_ran, lesson6_ran, break6_ran, lesson7_ran = true;

LiquidCrystal lcd(2, 3, 4, 5, 6, 8);
DS1302 rtc(A1, A2, A3);

void setup() {
    Serial.begin(9600);
    delay(1000);
    Serial.print(EEPROM.read(0)); // send data for pc program to pick up as default values
    Serial.print(",");
    Serial.print(EEPROM.read(1));
    Serial.print(",");
    Serial.print(EEPROM.read(2));
    Serial.print(",");
    Serial.print(EEPROM.read(3));
    pinMode(3, INPUT_PULLUP);
    pinMode(13, INPUT_PULLUP);
    pinMode(11, OUTPUT);
    rtc.halt(false);
    rtc.writeProtect(false);
    lcd.begin(16, 2);
    Time tyear = rtc.getTime();
    weli = tyear.year;
    update();
}

void update(){ // update the variables after duration of lesson or break has been changed
    lesson1_ran = false; finished = false;
    break1_ran = true; lesson2_ran = true; break2_ran = true; lesson3_ran = true; break3_ran = true; lesson4_ran = true; break4_ran = true; lesson5_ran = true; break5_ran = true; lesson6_ran = true; break6_ran = true; lesson7_ran = true;
    previous = 0;
    lessonrt = EEPROM.read(0);
    breakrt = EEPROM.read(1);
    bellrt = EEPROM.read(2);
    longbellrt = EEPROM.read(3);
    backup_method_enabled =  EEPROM.read(4); // in case time module breaks, if this is set to 1 on every startup time will be set to 9AM, otherwise it will wait for input from computer
    fullBreak = 300;
    bellms = bellrt * 1000;         //  stored as seconds, converted to milliseconds
    longbellms = longbellrt * 1000; //
    fullLesson = lessonrt * 60;         // stored as minutes, converted to seconds
    fullLongBreak = breakrt * 60;       //
    if (weli == 2165) {
        lcd.setCursor(0, 0);
        lcd.print("dros vegar");
        lcd.setCursor(0, 1);
        lcd.print("inaxavs");
        delay(2000);
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("me mitxarit");
        delay(2000);
        if (backup_method_enabled == true){
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("Yovel Chartvaze");
            lcd.setCursor(0, 1);
            lcd.print("Dro Dayendeba");
            delay(2000);
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("9:00 orshabatze");
            delay(2000);
            lcd.clear();
            broken_time = true;
        } else {
            lcd.setCursor(0, 0);
            lcd.print('chartet programa');
            lcd.setCursor(0, 1);
            lcd.print('kompiuterze');
        }
    } else {
        if (weli == 2000){
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("Bataria dajda");
            delay(3000);
            if (backup_method_enabled == true){
                lcd.setCursor(0, 0);
                lcd.print("Yovel Chartvaze");
                lcd.setCursor(0, 1);
                lcd.print("Dro Dayendeba");
                delay(3000);
                lcd.clear();
                lcd.setCursor(0, 0);
                lcd.print("9:00 orshabatze");
                delay(2000);
                lcd.clear();
                rtc.setDate(22, 7, 2002);
                rtc.setTime(9, 0, 0);
                rtc.setDOW(MONDAY);
                update();
            } else {
                lcd.setCursor(0, 0);
                lcd.print('chartet programa');
                lcd.setCursor(0, 1);
                lcd.print('kompiuterze');
            }
        }
    }
}
long get_time() { // returns seconds since 9 am (start time)
    long time_since_9am;
    if (broken_time != true){
        Time tm = rtc.getTime(); 
        if (tm.hour >= 9) {
            long hour_s = tm.hour * 60;
            hour_s = hour_s * 60; 
            int minute_s = tm.min * 60; 
            time_since_9am = hour_s + minute_s + tm.sec - 32399;
        } else {
            time_since_9am = 0;
        }
    } else {
        time_since_9am = millis() / 1000;
    }
    return time_since_9am;
}

String get_dow(){
    String output;
	Time tm = rtc.getTime();
	switch (tm.dow){
		case MONDAY:
			output="Orshabati";
			break;
		case TUESDAY:
			output="Samshabati";
			break;
		case WEDNESDAY:
			output="Otxshabati";
			break;
		case THURSDAY:
			output="Xutshabati";
			break;
		case FRIDAY:
			output="Paraskevi";
			break;
		case SATURDAY:
			output="Shabati";
			break;
		case SUNDAY:
			output="Kvira";
			break;
	}
	return output;
}


bool plus_minus(int actual_number, int number){ // resistance readings isn't correct, it's always little bit more or less 
    if (actual_number > number){
        if (actual_number - number <= 5){
            return true;
        } else {
            return false;
        }
    } else {
        if (number - actual_number <= 5){
            return true;
        } else {
            return false;
        }
    }
}

void ring(int duration = bellms){
    digitalWrite(11, HIGH);
    delay(duration);
    digitalWrite(11, LOW);
}

void loop() {
    if (Serial.available() > 0){ // see pc program's source code to understand this bs
        String received = Serial.readString();
        if (received.substring(0, 4) == "time"){
            rtc.setDate(received.substring(13, 15).toInt(), received.substring(10, 12).toInt(), received.substring(5, 9).toInt());
            rtc.setTime(received.substring(16, 18).toInt(), received.substring(19, 21).toInt(), received.substring(22, 24).toInt());
            switch (received.substring(25, 26).toInt()){
                case 0: rtc.setDOW(MONDAY);
                        break;
                case 1: rtc.setDOW(TUESDAY);
                        break;
                case 2: rtc.setDOW(WEDNESDAY);
                        break;
                case 3: rtc.setDOW(THURSDAY);
                        break;
                case 4: rtc.setDOW(FRIDAY);
                        break;
                case 5: rtc.setDOW(SATURDAY);
                        break;
                case 6: rtc.setDOW(SUNDAY);
                        break;
            }
            lcd.clear();
            lcd.print("dro dayenda");
            delay(1000);
        } else if (received.substring(0, 4) == "drtn"){
            EEPROM.update(0, received.substring(5, 7).toInt());
            EEPROM.update(1, received.substring(8, 10).toInt());
            EEPROM.update(2, received.substring(11, 13).toInt());
            EEPROM.update(3, received.substring(14, 16).toInt());
        } else if (received == "enable_backup_method"){
            EEPROM.update(4, 1);
        } else if (received == "disable_backup_method"){
            EEPROM.update(4, 0);
        } else if (received == "backdoor"){ // lol don't mind this
            ring();
        }
        update();
    }
    while (plus_minus(analogRead(0), 44)) { // prints time on lcd when button is pressed
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print(rtc.getTimeStr());
        lcd.setCursor(0, 1);
        lcd.print(get_dow());
        delay(1000);
    }

    if (plus_minus(analogRead(0), 125)) { // change time manually
        Time tm = rtc.getTime(); 
        int h = tm.hour;
        int m = tm.min;
        int s = tm.sec;
        delay(2000);
        while (true){
            delay(100);
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print(h);
            lcd.print(":");
            lcd.print(m);
            lcd.print(":");
            lcd.print(s);
            if (plus_minus(analogRead(0), 17)){ // increase
                ++m;
                if(m == 60){
                    m = 0;
                    ++h;
                }
                delay(100);
            } else if (plus_minus(analogRead(0), 28)){ // decrease
                --m;
                if(m == -1){
                    m = 59;
                    --h;
                }
                delay(100);
            } else if (plus_minus(analogRead(0), 125)) { // save
                rtc.setTime(h, m, s);
                break;
            }
        
        }
    } 

    if (plus_minus(analogRead(0), 365)) { // set lesson duration
        delay(2000);
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Gakvetili");
        while (true){
            delay(100);
            lcd.setCursor(0, 1);
            lcd.print(lessonrt);
            lcd.print("  ");
            if (plus_minus(analogRead(0), 17)){ // increase
                ++lessonrt;
                delay(100);
            } else if (plus_minus(analogRead(0), 28) && lessonrt != 0){ // decrease
                --lessonrt;
                delay(100);
            } else if (plus_minus(analogRead(0), 125)) { // save
                EEPROM.update(0, lessonrt);
                break;
            }
        
        }
        update();
    }

    if (plus_minus(analogRead(0), 157)) { // set long break duration
        delay(2000);
        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Dasveneba");
        while (true){
            delay(100);
            lcd.setCursor(0, 1);
            lcd.print(breakrt);
            lcd.print("  ");
            if (plus_minus(analogRead(0), 17)){ // increase
                ++breakrt;
                delay(100);
            } else if (plus_minus(analogRead(0), 28) && breakrt != 0){ // decrease
                --breakrt;
                delay(100);
            } else if (plus_minus(analogRead(0), 125)) { // save
                EEPROM.update(1, breakrt);
                break;
            }
        
        }
        update();
    }

    if (get_time() > 0 && get_time() <= (fullLesson * 7) + (fullBreak *5) + fullLongBreak + 10 && get_dow() != "Shabati" && get_dow() != "Kvira"){ // if time is more than 9am and day of the week isn't saturday or sunday
            if (pre == false) { // this executes only once
                lcd.setCursor(0, 0);
                lcd.print("Gakvetili - ");
                lcd.print(lessonrt);
                lcd.print("Wt");
                lcd.setCursor(0, 1);
                lcd.print("Didi Dasv - ");
                lcd.print(breakrt);
                lcd.print("Wt");
                delay(1000);
                lcd.clear();
                pre = true;
                lesson1_ran = false;
            }

            if (lesson1_ran == false){
                if (get_time() <= 10){    
                    ring(longbellms);
                }
                target_time = fullLesson; // time lesson is over
                if (get_time() < target_time){
                    currentState = "I Gakvetili";
                    current = 1;
                }
                lesson1_ran = true;
                break1_ran = false;
            } else if (break1_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){ // so it wouldn't ring if device was started in middle of lesson  
                    ring();
                }
                target_time = fullLesson + fullBreak;
                if (get_time() < target_time){
                    currentState = "I Dasveneba";
                    current = 2;
                }
                break1_ran = true;
                lesson2_ran = false;
            } else if (lesson2_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                };
                target_time = (fullLesson * 2) + fullBreak;
                if (get_time() < target_time){
                    currentState = "II Gakvetili";
                    current = 1;
                }
                lesson2_ran = true;
                break2_ran = false;
            } else if (break2_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                };
                target_time = (fullLesson * 2) + (fullBreak * 2);
                if (get_time() < target_time){
                    currentState = "II Dasveneba";
                    current = 2;
                }
                break2_ran = true;
                lesson3_ran = false;
            } else if (lesson3_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                };
                target_time = (fullLesson * 3) + (fullBreak * 2);
                if (get_time() <= target_time + fullLongBreak){
                    currentState = "III Gakvetili";
                    current = 1;
                }
                lesson3_ran = true;
                break3_ran = false;
            } else if (break3_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                };
                target_time = (fullLesson * 3) + (fullBreak * 2) + fullLongBreak;
                if (get_time() < target_time){
                    currentState = "Didi Dasveneba";
                    current = 3;
                }
                break3_ran = true;
                lesson4_ran = false;
            } else if (lesson4_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring(longbellms);
                };
                target_time = (fullLesson * 4) + (fullBreak * 2) + fullLongBreak;
                if (get_time() < target_time){
                    currentState = "IV Gakvetili";
                    current = 1;
                }
                lesson4_ran = true;
                break4_ran = false;
            } else if (break4_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                };
                target_time = (fullLesson * 4) + (fullBreak *3) + fullLongBreak;
                if (get_time() < target_time){
                    currentState = "IV Dasveneba";
                    current = 2;
                }
                break4_ran = true;
                lesson5_ran = false;
            } else if (lesson5_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                }
                target_time = (fullLesson * 5) + (fullBreak * 3) + fullLongBreak;
                if (get_time() < target_time){
                    currentState = "V Gakvetili";
                    current = 1;
                }
                lesson5_ran = true;
                break5_ran = false;
            } else if (break5_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                };
                target_time = (fullLesson * 5) + (fullBreak *4) + fullLongBreak;
                if (get_time() < target_time){
                    currentState = "V Dasveneba";
                    current = 2;
                }
                break5_ran = true;
                lesson6_ran = false;
            } else if (lesson6_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                };
                target_time = (fullLesson * 6) + (fullBreak * 4) + fullLongBreak;
                if (get_time() < target_time){
                    currentState = "VI Gakvetili";
                    current = 1;
                }
                lesson6_ran = true;
                break6_ran = false;
            } else if (break6_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                };
                target_time = (fullLesson * 6) + (fullBreak *5) + fullLongBreak;
                if (get_time() < target_time){
                    currentState = "VI Dasveneba";
                    current = 2;
                }
                break6_ran = true;
                lesson7_ran = false;
            } else if (lesson7_ran == false && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring();
                };
                target_time = (fullLesson * 7) + (fullBreak *5) + fullLongBreak;
                if (get_time() < target_time){
                    currentState = "VII Gakvetili";
                    current = 1;
                }
                lesson7_ran = true;
                finished = true;
            } else if (finished == true && get_time() >= target_time) {
                if (get_time() <= target_time + 10){    
                    ring(longbellms);
                }
                lcd.clear();
                finished = false;
                delay(10000);
            } else {
                lcd.clear();
                lcd.setCursor(0, 0);
                lcd.print(currentState);
                lcd.setCursor(0, 1);
                lcd.print("Darcha ");
                switch (current){
                    case 1: len = fullLesson;
                            break;
                    case 2: len = fullBreak;
                            break;
                    case 3: len = fullLongBreak;
                            break;
                }
            }

            remainingtime = target_time - get_time();
            minutes = remainingtime / 60;
            lcd.print(minutes);
            lcd.print(":");
            lcd.print(remainingtime - (minutes * 60));
            if (currentState != ""){ // if device was started up in middle of lessons, it would execute all previous if statements (but not ringing) without delaying 1 second after each loop
                delay(1000);            
            }

    } else {
            delay(1000);
            lcd.clear();
            lcd.setCursor(0, 0);
            lcd.print("Gakvetilebi");
            lcd.setCursor(0, 1);
            lcd.print("Dasrulda");
            delay(1000);
            lcd.clear();
            lcd.setCursor(0, 0);
            if (!broken_time){
                lcd.print(rtc.getTimeStr());
                lcd.setCursor(0, 1);
                lcd.print(get_dow());
            } else {
                lcd.print("e56fa338");
                lcd.setCursor(0, 1);
                lcd.print("2bed05da");
            }
    }
}